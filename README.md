# get-metadata-from-wav

## Description

Module that find duplicated wav files.

## External Requirements

This module requieres FFMPEG:

1. Download and extract: https://github.com/BtbN/FFmpeg-Builds/releases
2. Add PATH.
3. Reboot PC.

## Pip Packages

- pydub

## Terminal Syntax

```
python get_metadata.py <WAV Folder>
```
