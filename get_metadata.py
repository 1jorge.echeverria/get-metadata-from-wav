"""
Module providing a function that find compare-wav.py unsuportted wav files.

Author: @1jorge.echeverria
sys.argv[1]: WAV Folder.

This module requieres FFMPEG:
1. Download and extract: https://github.com/BtbN/FFmpeg-Builds/releases
2. Add PATH.
3. Reboot PC.
"""

import sys
import os
import pathlib
from pydub.utils import mediainfo


def __is_wav_file(file):
    """
    Check if the file is a .wav file.

    Parameters:
        file (str): File's full path.

    Returns:
        bool: True if file is .wav.
    """
    return pathlib.Path(file).suffix.lower() == '.wav'


def __have_other_audio_formats(file):
    """
    Check if the file is a .mp3, .aif, .aiff, .wv, .aac, .flac, .m4a, .ogg or .wma file.

    Parameters:
        file (str): File's full path.

    Returns:
        bool: True if file is .mp3, .aif, .aiff, .wv, .aac, .flac, .m4a, .ogg or .wma.
    """
    return pathlib.Path(file).suffix.lower() in ['.mp3', '.aif', '.aiff', '.wv', '.aac', '.flac', '.m4a', '.ogg', '.wma']


def __get_file_list(folder):
    """
    Creates a list with all files from folder.

    Parameters:
        folder (str): Folder's full path.

    Returns:
        list: Supported .wav files.
    """
    file_list = []
    unsupported_audio_files = []
    for dirpath, _, filenames in os.walk(folder):
        for filename in filenames:
            # Get file's full path.
            file = os.path.join(dirpath, filename)
            try:
                # Normalices file's path.
                file = os.path.normpath(file)
                # Add path to list.
                if __is_wav_file(file):
                    file_list.append(file)
                elif __have_other_audio_formats(file):
                    unsupported_audio_files.append(file)
            except UnicodeDecodeError:
                pass
    if unsupported_audio_files:
        print('\nFILES WITH OTHER EXTENSIONS:\n')
        for file1 in unsupported_audio_files:
            print(file1)
    return file_list


def __get_metadata_from_all_files(folder):
    """
    Shows the problematic wav files.

    Parameters:
        folder (str): Supported .wav files
    """
    print("\nWAV FILES TO CONVERT:\n")
    for file1 in folder:
        if not 'pcm_s' in mediainfo(file1)['codec_name']:
            print(file1)


if __name__ == "__main__":
    __get_metadata_from_all_files(__get_file_list(sys.argv[1]))
